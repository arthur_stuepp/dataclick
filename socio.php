<!DOCTYPE html>
<html>
<head>
	<title>Cadastrar sócio</title>
	<link rel="stylesheet" type="text/css" href="css/css/bootstrap.css">
</head>
<body>
			<?php include_once("header.php");?>
			<div class="secundaria">
			<legend>Cadastrar socio</legend>
			<form id="form" action="cadastrar_socio.php" method="POST">
			<label for="clube"> Nome completo</label>
				<input type="text" name="socio" id="socio" required>
				<table class="table">
				  <thead>
				    <tr>
				    <th scope="col">#</th>	
				      <th scope="col">clube</th>
				      <th scope="col">socio</th>
				  </thead>
				  <tbody>
				    <?php 
				    	include_once("conexao.php");				    
				    	$sql=("select idclube,nome_clube from clube");
						$query=mysqli_query($con,$sql);
						$linha=1;
					 	while ($clubes=mysqli_fetch_object($query))
					    {
					    		echo "<tr>";
					     		echo "<th scope='row'>$linha</th>";
					     		echo "<td>$clubes->nome_clube</td>";
					     		echo "<td><input type='checkbox' name='checkbox[]' value=$clubes->idclube> </td>";
					   			echo " </tr>";
								$linha++;
					    }
 					?>
				  </tbody>
				</table>
				<button id="btn_cadastrar" onclick="funcao()" class="btn btn-success">Cadastrar</button>
			</form>

			<?php
				if(!empty($_GET['status'])){
					if($_GET['status']=='success'){
						echo "<div class='alert alert-success'>
 								Cadastrado com Sucesso.
							</div>";
					}else if ($_GET['status']=='error'){
						echo "<div class='alert alert-danger'>
 								Erro ao cadastrar
							</div>";

						}
				}
			?>	
		</div>
</body>
</html>